'use strict';

var autoprefixer = require('gulp-autoprefixer');
var csso = require('gulp-csso');
var del = require('del');
var gulp = require('gulp');
var htmlmin = require('gulp-htmlmin');
var runSequence = require('run-sequence');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');

const imagemin = require('imagemin');
const imageminWebp = require('imagemin-webp');

// Set the browser that you want to supoprt
const AUTOPREFIXER_BROWSERS = [
  'ie >= 10',
  'ie_mob >= 10',
  'ff >= 30',
  'chrome >= 34',
  'safari >= 7',
  'opera >= 23',
  'ios >= 7',
  'android >= 4.4',
  'bb >= 10'
];

// Gulp task to minify CSS filescl
gulp.task('styles', function () {
  return gulp.src('./styles/**/*.css')
    // Compile SASS files
    .pipe(sass({
      outputStyle: 'nested',
      precision: 10,
      includePaths: ['.'],
      onError: console.error.bind(console, 'Sass error:')
    }))
    // Auto-prefix css styles for cross browser compatibility
    .pipe(autoprefixer({browsers: AUTOPREFIXER_BROWSERS}))
    // Minify the file
    .pipe(csso())
    // Output
    .pipe(gulp.dest('./dist/styles'))
});


// Gulp task to minify CSS filescl
gulp.task('styles2', function () {
  return gulp.src('./plugins/**/*.css')
    // Compile SASS files
    .pipe(sass({
      outputStyle: 'nested',
      precision: 10,
      includePaths: ['.'],
      onError: console.error.bind(console, 'Sass error:')
    }))
    // Auto-prefix css styles for cross browser compatibility
    .pipe(autoprefixer({browsers: AUTOPREFIXER_BROWSERS}))
    // Minify the file
    .pipe(csso())
    // Output
    .pipe(gulp.dest('./dist/plugins'))
});

// Gulp task to minify JavaScript files
gulp.task('scripts', function() {
  return gulp.src('./js/**/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('./dist/js'))
});

// Gulp task to minify JavaScript files
gulp.task('scripts2', function() {
  return gulp.src('./styles/**/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('./dist/styles'))
});

// Gulp task to minify JavaScript files
gulp.task('scripts3', function() {
  return gulp.src('./plugins/**/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('./dist/plugins'))
});

// Gulp task to minify JavaScript files
gulp.task('scripts4', function() {
  return gulp.src('./plugins/**/*.js')
    .pipe(uglify())
    .pipe(gulp.dest('./dist/plugins'))
});

// Gulp task to minify HTML files
gulp.task('pages', function() {
  return gulp.src(['./index.html'])
    .pipe(htmlmin({
      collapseWhitespace: true,
      removeComments: true
    }))
    .pipe(gulp.dest('./dist'));
});

gulp.task('images', function() {
  imagemin(['images/*.*'], 'dist/images', {
    use: [
        imageminWebp({quality: 80})
    ]
  })
});

// Clean output directory
gulp.task('clean', () => del(['dist']));

// Gulp task to minify all files
gulp.task('default', ['clean'], function () {
  runSequence(
    'styles',
    'styles2',
    'pages',
    'images',
    'scripts',
    'scripts2',
    'scripts3',
    'scripts4'
  );
});