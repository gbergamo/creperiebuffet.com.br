/* JS Document */

/******************************

[Table of Contents]

1. Vars and Inits
2. Set Header
3. Init Menu
4. Init Video
5. Init Date Picker
6. Init Time Picker


******************************/

$(document).ready(function(){
	"use strict";

	/* 

	1. Vars and Inits

	*/

	var header = $('.header');
	var hamburgerBar = $('.hamburger_bar');
	var hamburger = $('.hamburger');
	var menuItens = $('div.menu.trans_800 li');
	var btnReserva = $('#btnReserva');

	setHeader();

	$(window).on('resize', function(){
		setHeader();

		setTimeout(function()
		{
			$(window).trigger('resize.px.parallax');
		}, 375);
	});

	$(document).on('scroll', function()
	{
		setHeader();
	});

	initVideo();
	initDatePicker();
	initTimePicker();
	initMenu();
	initReserva();

	/* 

	2. Set Header

	*/

	function setHeader(){
		if($(window).scrollTop() > 91)
		{
			header.addClass('scrolled');
			hamburgerBar.addClass('scrolled');
		}
		else
		{
			header.removeClass('scrolled');
			hamburgerBar.removeClass('scrolled');
		}
	}

	/* 

	3. Init Menu

	*/

	function initMenu(){
		if($('.menu').length)
		{
			var menu = $('.menu');
			hamburger.on('click', function(){
				hamburger.toggleClass('active');
				menu.toggleClass('active');
			});

			menuItens.on('click', function(){
				hamburger.toggleClass('active');
				menu.toggleClass('active');
			});
		}
	}

	/* 

	4. Init Video

	*/

	function initVideo()
	{
		$(".vimeo").colorbox(
		{
			iframe:true,
			innerWidth:640,
			innerHeight:409,
			maxWidth: '90%'
		});
	}

	/* 

	5. Init Date Picker

	*/

	function initDatePicker()
	{
		var dp = $('#datepicker');
		var date = new Date();
		var dateM = date.getMonth() + 1;
		var dateD = date.getDate();
		var dateY = date.getFullYear();
		var dateFinal = dateD + '/' + dateM + '/' + dateY;
		dp.val(dateFinal);
		dp.datepicker({
			dateFormat: 'dd/mm/yy'
		});
	}

	/* 

	6. Init Time Picker

	*/

	function initTimePicker()
	{
		$('.timepicker').timepicker(
		{
		    interval: 60,
		    minTime: '10',
		    maxTime: '6:00pm',
		    defaultTime: '11',
		    startTime: '10:00',
		    dynamic:  true,
		    dropdown: true,
		    scrollbar: true
		});
	}

	/* 

	7. Init Reserva

	*/
	function initReserva(){	
		btnReserva.on('click', function(evt){
			evt.preventDefault();

			var date = $('#datepicker').val();
			var time = $('.timepicker').val();
			var peaple = $('.res_select').val() != null ? $('.res_select').val() : "5 pessoas";

			window.location.href = "https://api.whatsapp.com/send?phone=5511953716738&text=Olá gostaria de verificar a disponibilidade na seguinte data" + date + " " + " " + time + " para " + peaple + ".";

			return false;
		});
	}
});