build:
	gulp
	mkdir -p dist/font && cp -R ./font dist
	mkdir -p dist/images && cp -R ./images dist
	cp ./sitemap.xml dist
	cp ./robots.txt dist
	cp ./favicon.ico dist
	cp -R ./plugins/font-awesome-4.7.0/fonts  ./dist/plugins/font-awesome-4.7.0/fonts
	
deploy: build
	aws s3 sync ./dist s3://creperiebuffet.com.br --profile gbergamo --delete --acl public-read --cache-control='max-age=3888000'
	aws cloudfront create-invalidation --profile gbergamo --distribution-id EYI2CT676TRRP --paths /index.html /css/* /images/* /fonts/* /img/* /js/* /plugins/* /styles/*
	